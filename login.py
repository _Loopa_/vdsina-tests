import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import xmlrunner
import time


class LoginTest(unittest.TestCase):
    def setUp(self):
        options = webdriver.ChromeOptions()
        capabilities = options.to_capabilities()
        self.driver = webdriver.Remote(command_executor='http://selenium:4444/wd/hub',
                                       desired_capabilities=capabilities)

    def test_login(self):
        driver = self.driver
        driver.get("https://cp.vdsina.ru/")
        driver.implicitly_wait(5)
        driver.save_screenshot('Screenshots/docker_image_1.png')
        login = driver.find_element_by_id("email")
        login.send_keys("kenneth@list.ru")
        login.send_keys(Keys.RETURN)
        password = driver.find_element_by_id("password")
        password.send_keys("ZE6xed882W")
        password.send_keys(Keys.RETURN)
        # driver.implicitly_wait(10)
        time.sleep(5)
        # driver.find_element_by_xpath('//*[@id="app"]/div[1]/fieldset/div[4]').click()
        driver.implicitly_wait(5)
        self.assertIn('Панель управления', driver.title)
        driver.save_screenshot('Screenshots/control_panel.png')

    def tearDown(self):
        driver = self.driver
        driver.close()
        driver.quit()


if __name__ == '__main__':
    with open('report.xml', 'wb') as output:
        unittest.main(
            testRunner=xmlrunner.XMLTestRunner(output=output),
            failfast=False, buffer=False, catchbreak=False)
